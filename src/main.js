import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';
import PizzaForm from './pages/PizzaForm';
import { doc } from 'prettier';

const pizzaList = new PizzaList([]),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];
pizzaList.pizzas = data;
Router.navigate(document.location.pathname); // affiche la liste des pizzas
Router.menuElement = document.querySelector('.mainMenu');

document.querySelector('.logo').innerHTML +=
	"<small> Les pizzas c'est la vie</small>";

document.querySelector('li a').classList.add('active');

const news = document.querySelector('.newsContainer');
news.setAttribute('style', "display:''");
const close = document.querySelector('.newsContainer button');
function handleClick(event) {
	event.preventDefault();
	news.setAttribute('style', 'display:none');
}
close.addEventListener('click', handleClick);

window.onpopstate = e => {
	console.log(document.location);
	Router.navigate(document.location.pathname, false);
};
window.onpopstate();
