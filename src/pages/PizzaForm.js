import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const button = element.querySelector('button');
		button.addEventListener('click', this.submit);
	}

	submit(event) {
		event.preventDefault();
		const form = document.querySelector('form'),
			input = form.querySelector('input[name=name]');
		if (!input.value == '') {
			alert(`La pizza ${input.value} a été créer`);
		} else alert('Veuillez entrer un nom de pizza');
	}
}
